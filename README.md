Android Application Using Github API<br />

Has 2 fragment :<br />
    1- list fragment<br />
    2- detail fragment

List fragment contains endless recycler view that shows database base content
or data that fetched from Github API.<br />

Details fragment shows some details of a single project<br />

Project using<br />
    1- Room<br />
    2- Volley<br />
    3- MVVM architecture<br />
